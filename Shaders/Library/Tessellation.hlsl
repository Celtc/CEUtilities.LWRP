#ifndef TESSELLATION_INCLUDED
#define TESSELLATION_INCLUDED

#include "Packages/com.unity.render-pipelines.lightweight/ShaderLibrary/Core.hlsl"
#include "Library/Blur.hlsl"

// Define a macro, used to set the maximum tessellation factor
#define MAX_TESSELLATION_FACTORS 64.0 

#if TESSELLATION_POSITION_OS
#define HAS_UNITY_POSITION 1
#define UNITY_POSITION positionOS
#elif TESSELLATION_POSITION
#define HAS_UNITY_POSITION 1
#define UNITY_POSITION position
#endif

#if TESSELLATION_NORMAL_OS
#define HAS_UNITY_NORMAL 1
#define HAS_UNITY_OUTPUT_NORMAL 1
#define UNITY_NORMAL normalOS
#elif TESSELLATION_NORMAL
#define HAS_UNITY_NORMAL 1
#define HAS_UNITY_OUTPUT_NORMAL 1
#define UNITY_NORMAL normal
#else
#define HAS_UNITY_NORMAL 1
#define UNITY_NORMAL normalOS
#endif

#if TESSELLATION_TEXCOORD
#define HAS_UNITY_UV0 1
#define UNITY_UV0 texcoord
#elif TESSELLATION_UV0
#define HAS_UNITY_UV0 1
#define UNITY_UV0 uv0
#endif

#if TESSELLATION_LIGHTMAP_UV
#define HAS_UNITY_UV1 1
#define UNITY_UV1 lightmapUV
#elif TESSELLATION_UV1
#define HAS_UNITY_UV1 1
#define UNITY_UV1 uv1
#endif

#if TESSELLATION_UV2
#define HAS_UNITY_UV2 1
#define UNITY_UV2 uv2
#endif

#if TESSELLATION_TANGENT_OS
#define HAS_UNITY_TANGENT 1
#define UNITY_TANGENT tangentOS
#endif

// The input structure of the tessellation
struct TAttributes
{
#if HAS_UNITY_POSITION
	float4 UNITY_POSITION	: POSITION;
#endif
#if HAS_UNITY_NORMAL
	float3 UNITY_NORMAL		: NORMAL;
#endif
#if HAS_UNITY_TANGENT
	float4 UNITY_TANGENT	: TANGENT;
#endif
#if HAS_UNITY_UV0
	float2 UNITY_UV0		: TEXCOORD0;
#endif
#if HAS_UNITY_UV1
	float2 UNITY_UV1		: TEXCOORD1;
#endif
#if HAS_UNITY_UV2
	float2 UNITY_UV2		: TEXCOORD2;
#endif

	UNITY_VERTEX_INPUT_INSTANCE_ID
};

// The output structure of the vertex shader
struct TessellationControlPoint
{
#if HAS_UNITY_POSITION
	float4 UNITY_POSITION	: INTERNALTESSPOS;
#endif
#if HAS_UNITY_NORMAL
	float3 UNITY_NORMAL		: NORMAL;
#endif
#if HAS_UNITY_TANGENT
	float4 UNITY_TANGENT	: TANGENT;
#endif
#if HAS_UNITY_UV0
	float2 UNITY_UV0		: TEXCOORD0;
#endif
#if HAS_UNITY_UV1
	float2 UNITY_UV1		: TEXCOORD1;
#endif
#if HAS_UNITY_UV2
	float2 UNITY_UV2		: TEXCOORD2;
#endif

	UNITY_VERTEX_INPUT_INSTANCE_ID
};

// The structure of the surface subdivision factor, the general structure, and the UnityTessellationFactor structure defined in the unity buildIn shader
struct TessellationFactors
{
	float edge[3]	: SV_TessFactor;		//The subdivision factor of the three sides of the triangle
	float inside	: SV_InsideTessFactor; // The subdivision factor inside the triangle
};

// Tessellation Vertex shader
TessellationControlPoint TessellatedVertex(TAttributes input)
{
	TessellationControlPoint output;

	UNITY_SETUP_INSTANCE_ID(input);
	UNITY_TRANSFER_INSTANCE_ID(input, output);

#if HAS_UNITY_POSITION
	output.UNITY_POSITION = input.UNITY_POSITION;
#endif
#if HAS_UNITY_NORMAL
	output.UNITY_NORMAL = input.UNITY_NORMAL;
#endif
#if HAS_UNITY_TANGENT
	output.UNITY_TANGENT = input.UNITY_TANGENT;
#endif
#if HAS_UNITY_UV0
	output.UNITY_UV0 = input.UNITY_UV0;
#endif
#if HAS_UNITY_UV1
	output.UNITY_UV1 = input.UNITY_UV1;
#endif
#if HAS_UNITY_UV2
	output.UNITY_UV2 = input.UNITY_UV2;
#endif

	return output;
}

// Clipping function
bool TriangleIsBelowClipPlane(float3 p0, float3 p1, float3 p2, int planeIndex, float bias) 
{
	float4 plane = unity_CameraWorldClipPlanes[planeIndex];
	return
		dot(float4(p0, 1), plane) < bias &&
		dot(float4(p1, 1), plane) < bias &&
		dot(float4(p2, 1), plane) < bias;
}

// Triangle clip checker function
bool TriangleIsCulled(float3 p0, float3 p1, float3 p2, float bias) 
{
	return
		TriangleIsBelowClipPlane(p0, p1, p2, 0, bias) ||
		TriangleIsBelowClipPlane(p0, p1, p2, 1, bias) ||
		TriangleIsBelowClipPlane(p0, p1, p2, 2, bias) ||
		TriangleIsBelowClipPlane(p0, p1, p2, 3, bias);
}

// Calculate tessellation factor
float TessellationFactor(float3 p0, float3 p1, float2 uv0, float2 uv1)
{
#if _TESSELLATIONMAP
	float2 interpolatedUV = TRANSFORM_TEX(((uv0 + uv1) * 0.5).xy, TESSELLATION_TRANSFORM_SOURCE);
	float tessellation = SampleTessellation(interpolatedUV);
#else
	float tessellation = SampleTessellation();
#endif

#if _TESSELLATION_EDGE
	float edgeLength = distance(p0, p1);
	float3 edgeCenter = (p0 + p1) * 0.5;
	float viewDistance = max(0.0, distance(edgeCenter, _WorldSpaceCameraPos) - _TessellationEdgeDistanceOffset);
	float factor = edgeLength * _ScreenParams.y / (_TessellationEdgeLength * viewDistance);
#elif _TESSELLATION_DISTANCE
	float3 edgeCenter = (p0 + p1) * 0.5;
	float viewDistance = max(0.0, distance(edgeCenter, _WorldSpaceCameraPos));
	float rawFactor = clamp(1.0 - (viewDistance - _TessellationDistanceMin) / (_TessellationDistanceMax - _TessellationDistanceMin), 0.01, 1.0);
	float factor = (_TessellationFactorMin - _TessellationFactorMax) * rawFactor + _TessellationFactorMax;
#else
	float factor = _TessellationFactor;
#endif
	return min(factor * tessellation, MAX_TESSELLATION_FACTORS);
}

// Patch function
TessellationFactors HullConstant(InputPatch<TessellationControlPoint, 3> input)
{
	UNITY_SETUP_INSTANCE_ID(input[0]);

	float3 p0 = mul(unity_ObjectToWorld, input[0].UNITY_POSITION).xyz;
	float3 p1 = mul(unity_ObjectToWorld, input[1].UNITY_POSITION).xyz;
	float3 p2 = mul(unity_ObjectToWorld, input[2].UNITY_POSITION).xyz;

	TessellationFactors output;

	float bias = -_TessellationTriangleClipBias * _HeightStrength * 2.0;
	if (TriangleIsCulled(p0, p1, p2, bias)) 
	{
		output.edge[0] = output.edge[1] = output.edge[2] = output.inside = 0;
	}
	else
	{
		output.edge[0] = TessellationFactor(p1, p2, input[1].UNITY_UV0, input[2].UNITY_UV0);
		output.edge[1] = TessellationFactor(p2, p0, input[2].UNITY_UV0, input[0].UNITY_UV0);
		output.edge[2] = TessellationFactor(p0, p1, input[0].UNITY_UV0, input[1].UNITY_UV0);
		output.inside =
				(output.edge[0] +
				output.edge[1] +
				output.edge[2]) / 3.0;
	}

	return output;
}

// Hull shader
[domain("tri")]						// Processing triangle face
[partitioning("fractional_odd")]	// The parameter type of the subdivided factor, can be "integer" which is used to represent the integer, or can be a floating point number "fractional_odd"
[outputtopology("triangle_cw")]		// Clockwise vertex arranged as the front of the triangle
[patchconstantfunc("HullConstant")] // The function that calculates the factor of the triangle facet is not a constant. Different triangle faces can have different values. A constant can be understood as a uniform value for the three vertices inside a triangle face.
[outputcontrolpoints(3)]			// Explicitly point out that each patch handles three vertex data
TessellationControlPoint TessellatedHull(
	InputPatch<TessellationControlPoint, 3> patch,
	uint id:SV_OutputControlPointID)
{
	// Direct output here, calculate the subdivision factor is in the specified HullConstant function
	return patch[id]; 
	// Here you can do other pre-tessellation calculations
}

// Domain Program
[domain("tri")]	// Specified to handle the triangle face triangle
Varyings TessellatedDomain(
	TessellationFactors tessFactors,
	const OutputPatch<TessellationControlPoint, 3> input,
	float3 barycentricCoordinates : SV_DomainLocation)
{
	Attributes data = (Attributes)0;

	UNITY_TRANSFER_INSTANCE_ID(input[0], data);

	// This uses a macro definition to reduce the writing of duplicate code
#define MY_DOMAIN_PROGRAM_INTERPOLATE(fieldName) input[0].fieldName * barycentricCoordinates.x + \
					input[1].fieldName * barycentricCoordinates.y + \
					input[2].fieldName * barycentricCoordinates.z;

#if HAS_UNITY_POSITION
	data.UNITY_POSITION = MY_DOMAIN_PROGRAM_INTERPOLATE(UNITY_POSITION)	// Interpolation calculates vertex coordinates
#endif
#if HAS_UNITY_NORMAL
	float3 outputNormal = MY_DOMAIN_PROGRAM_INTERPOLATE(UNITY_NORMAL)
#if HAS_UNITY_OUTPUT_NORMAL
	data.UNITY_NORMAL = outputNormal;									// Interpolation calculation normal
#endif
#endif
#if HAS_UNITY_TANGENT
	data.UNITY_TANGENT = MY_DOMAIN_PROGRAM_INTERPOLATE(UNITY_TANGENT)	// Interpolation calculation tangent
#endif
#if HAS_UNITY_UV0
	data.UNITY_UV0 = MY_DOMAIN_PROGRAM_INTERPOLATE(UNITY_UV0);			// Interpolation calculation UV0
#endif
#if HAS_UNITY_UV1
	data.UNITY_UV1 = MY_DOMAIN_PROGRAM_INTERPOLATE(UNITY_UV1)			// Interpolation calculation UV1
#endif
#if HAS_UNITY_UV2
	data.UNITY_UV2 = MY_DOMAIN_PROGRAM_INTERPOLATE(UNITY_UV2)			// Interpolation calculation UV2
#endif

#if _HEIGHTMAP && HAS_UNITY_NORMAL && HAS_UNITY_UV0
	float2 samplingUV = TRANSFORM_TEX(data.UNITY_UV0, TESSELLATION_TRANSFORM_SOURCE);
	data.UNITY_POSITION.xyz += normalize(outputNormal) * SampleHeight(samplingUV);
#endif

	return PassVertexProgram(data);					// Process the interpolation results, prepare the data needed in the rasterization phase
}

#endif
